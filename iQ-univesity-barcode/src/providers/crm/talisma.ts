import { Injectable } from '@angular/core';
import { Events } from './events';
import { Participants } from './participants';
import { Speakers } from './speakers';

@Injectable()
export class Talisma {
  constructor(
    public events: Events,
    public participants: Participants,
    public speakers: Speakers
  ) { }
}
