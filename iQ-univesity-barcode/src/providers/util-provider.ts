import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController, ActionSheetController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Camera } from 'ionic-native';
import * as firebase from 'firebase';
import * as _ from 'underscore';

@Injectable()
export class UtilProvider {
  loaderInstance: any = null;
  toastInstance: any = null;
  constructor(
    public actionSheetCtrl: ActionSheetController,
    public AlertCtrl: AlertController,
    public platform: Platform,
    public local: Storage,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) { }

  // show Loading UI with message
  showLoading(message: string) {
    this.loaderInstance = this.loadingCtrl.create({
      content: message || 'Loading...',
    });
    this.loaderInstance.present();
    return this.loaderInstance;
  }

  // hide the Loading UI
  hideLoading() {
    if (this.loaderInstance !== null) {
      this.loaderInstance.dismiss();
      this.loaderInstance = null;
    }
  }

  // show Toast UI with message
  showToast(params: any) {
    this.toastInstance = this.toastCtrl.create(params);
    this.toastInstance.present();
    return this.toastInstance;
  }

  // hide the Toast UI
  hideToast() {
    if (this.toastInstance !== null) {
      this.toastInstance.dismiss();
      this.toastInstance = null;
    }
  }

  // clean the attributes which comes as part of offline data
  cleanOfflineDataAttrs(data: any, others?: any) {
    others = (_.defaults([], others)).concat(['$key', '$exists'])
    _.each(others, (v, k) => {
      delete data[k];
    })

    return data;
  }

  isEmpty(value: any) {
    let flag: boolean = true;
    try {
      flag = (value === undefined) || (value.toString().length < 1);
    } catch (e) { }
    return flag;
  }

  getUid() {
    return this.local.get('uid');
  }

  doAlert(title: string, message: string, buttonText: any) {
    let alert = this.AlertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [buttonText]
    });
    return alert;
  }

  getPicture() {
    let options = {
      quality: 50,
      allowEdit: true,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: Camera.PictureSourceType.CAMERA
    };

    let promise = new Promise((resolve, reject) => {
      this.actionSheetCtrl.create({
        title: 'Options',
        buttons: [
          {
            text: 'From Gallery',
            icon: !this.platform.is('ios') ? 'images' : null,
            handler: () => {
              options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
              Camera.getPicture(options)
                .then(function(image) {
                  resolve(image);
                }, reject);
            }
          }, {
            text: 'From Camera',
            icon: !this.platform.is('ios') ? 'camera' : null,
            handler: () => {
              Camera.getPicture(options)
                .then(function(image) {
                  resolve(image);
                }, reject);
            }
          }, {
            text: 'Cancel',
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel',
            handler: () => {
              reject('Cancel clicked');
            }
          }
        ]
      }).present();
    });
    return promise;
  }

  savePicture(filename: any, folder: string, image: any) {
    let loader: any;
    return new Promise((resolve, reject) => {
      loader = this.loadingCtrl.create({
        content: "Image is uploading... please wait",
      });
      loader.present();

      firebase.storage().ref().child(`${folder}/${filename}.jpg`).putString(image, 'base64')
        .then((snapshot) => {
          let toast = this.toastCtrl.create({
            message: 'Image uploaded successfully',
            duration: 3000
          });
          toast.present();
          loader.dismiss();
          resolve(snapshot.downloadURL);
        })
        .catch((error) => {
          console.log('Image upload failed ' + error)
          loader.dismiss();
          reject(error);
        });
    });
  };
}
