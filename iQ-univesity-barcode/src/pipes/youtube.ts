import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'youtube' })

export class YouTube implements PipeTransform {

  constructor(
    private domSanitizer: DomSanitizer
  ) { }

  transform(value: any): any {

    let video_id = value.split("v=")[1] || value;
    let ampersand_pos = video_id.indexOf("&");
    if (ampersand_pos != -1) {
      video_id = video_id.substring(0, ampersand_pos)
    }
    value = 'https://www.youtube.com/embed/' + video_id + '?showinfo=0&rel=0';
    return this.domSanitizer.bypassSecurityTrustResourceUrl(value);

  }
}
