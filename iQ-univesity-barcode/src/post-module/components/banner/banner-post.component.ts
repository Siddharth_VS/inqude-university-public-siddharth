import { Component, Input } from '@angular/core';
import { PostComponent } from '../../post.component';
import { HelperService } from '../../../providers/helperService';

@Component({
  templateUrl: 'banner-post.component.html',
  selector: 'component-banner-post',
  providers: [HelperService]
})
export class BannerPostComponent implements PostComponent {
  @Input() post: any;
  @Input() params: any;

  constructor(public helper: HelperService)  {}
}
