import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { FileUploader } from '../../pages/file-uploader/file-uploader';
import { ActionSheetController, ModalController, Platform, reorderArray } from 'ionic-angular';

@Component({
  selector: 'image-gallery',
  templateUrl: 'image-gallery.html'
})

export class ImageGallery {

  @Input('params') params: any;
  @Output() updateMenu = new EventEmitter();
  images: any[] = [];
  edit: boolean = false;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public afoDatabase: AngularFireOfflineDatabase,
    public modalCtrl: ModalController,
    public platform: Platform,
  ) { }

  ngOnChanges() {
    if (this.params && this.params.images) {
      this.images = this.params.images;
    }
  }

  attachImage() {
    let modal = this.modalCtrl.create(FileUploader, { folder: this.params.folder, allowMultipleSelection: true });
    modal.present();
    modal.onDidDismiss((data: any) => {
      if (data && data.length && data.length > 0) {
        data.forEach((item: any) => {
          if (this.images.indexOf(item) == -1) {
            this.images.push(item);
            this.updateMenu.emit({ menuName: "IMAGE_SLIDER" });
          }
        })
      }
    })
  }

  onItemReorder(indexes: any) {
    this.images = reorderArray(this.images, indexes);
    this.updateMenu.emit({ menuName: "IMAGE_SLIDER" });
  }

  showOptions(item: any, index: number) {
    this.actionSheetCtrl.create({
      title: 'options',
      buttons: [
        {
          text: 'Delete',
          icon: !this.platform.is('ios') ? 'trash' : null,
          role: 'destructive',
          handler: () => {
            this.images.splice(index, 1);
            this.updateMenu.emit({ menuName: "IMAGE_SLIDER" });
          }
        },
        {
          text: 'Cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
        }
      ]
    }).present();
  }

}
