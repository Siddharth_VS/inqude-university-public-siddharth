import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  template: `
  <ion-list radio-group [(ngModel)]="filter" style="margin:0">
<ion-item>
  <ion-label>Upcoming</ion-label>
  <ion-radio value="live" (ionSelect)="close($event)"></ion-radio>
</ion-item>
<ion-item>
  <ion-label>Past</ion-label>
  <ion-radio value="past" (ionSelect)="close($event)"></ion-radio>
</ion-item>
</ion-list>
  `
})
export class EventFilter {
  filter: any;
  constructor(
    public viewCtrl: ViewController,
    public nav: NavParams
  ) {
    this.filter = this.nav.get('filter');
  }

  close(option: any) {
    this.viewCtrl.dismiss(option);
  }
}
