import { Component, Input } from "@angular/core";
import { NavController } from "ionic-angular";
import { EventDetailPage } from "../../pages/event-detail/event-detail";
import { UserData } from "../../providers/user-data";
import { AngularFireOfflineDatabase } from "angularfire2-offline";
import * as moment from "moment-timezone";

@Component({
  selector: "single-event",
  templateUrl: "single-event.html"
})
export class SingleEvent {
  @Input("params") eventData: any;
  @Input("data") paramsData: any;
  timeZone: string = "America/Matamoros";
  roleValue: any;
  event: any = {};
  ParticipationCount: any;
  universityRef: any = "";
  moment = moment;

  constructor(private afoDatabase: AngularFireOfflineDatabase, private navCtrl: NavController, private userData: UserData) {}
  ngOnChanges() {
    this.afoDatabase
      .object("/settings/timeZone")
      .take(1)
      .subscribe((timeZone: any) => {
        this.timeZone = timeZone.$value;
        if (this.eventData && this.paramsData && this.paramsData.fetchLatestData) {
          this.userData
            .getRoleValue()
            .then((roleValue) => {
              this.roleValue = roleValue;
              return this.userData.getUniversityId();
            })
            .then((universityId) => {
              this.universityRef = "universitiesData/" + universityId;
              if (this.roleValue == 20) {
                this.afoDatabase.object(this.universityRef + "/events/" + this.eventData.EventId).subscribe((event) => {
                  this.event = event;
                });
                this.afoDatabase.object(this.universityRef + "/participants/" + this.eventData.EventId).subscribe((participants) => {
                  this.ParticipationCount = participants.length || 0;
                });
              } else {
                this.event = this.eventData;
              }
            });
        } else {
          this.event = this.eventData;
        }
      });
  }
  getClass(event: any) {
    return moment(event.EventStartDate)
      .tz(this.timeZone)
      .format("x") >=
      moment()
        .tz(this.timeZone)
        .subtract(1, "days")
        .endOf("day")
        .format("x")
      ? ""
      : "past-event";
  }

  viewEvent(eventData: any) {
    this.navCtrl.push(EventDetailPage, { event: eventData });
  }
}
