import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { App } from 'ionic-angular';
import { SpeakerDetailPage } from '../../pages/speaker/speaker-detail/speaker-detail';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'persona',
  templateUrl: 'persona.html'
})
export class Persona {
  @Input('params') args: any;
  @Output() removePersona = new EventEmitter();
  persona?: any[] = [];
  universityRef: any = '';

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    public userData: UserData
  ) { }

  ngOnChanges() {
    let self = this;
    this.userData.getUniversityId()
      .then((universityId) => {
        self.universityRef = 'universitiesData/' + universityId;
        self.afoDatabase.object(self.universityRef + '/speakers/' + this.args.id)
          .subscribe((speaker) => {
            self.persona = speaker;
          });
      })

  }

  viewPersona(persona: any) {
    if (this.args.view == true) {
      this.app.getRootNav().push(SpeakerDetailPage, { speakerId: persona.id });
    }
  }

  deletePersona(personaId: any) {
    this.removePersona.emit({ id: personaId, type: this.args.type });
  }
}
