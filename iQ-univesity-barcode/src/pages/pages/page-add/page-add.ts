import { Component } from '@angular/core';
import { App, AlertController, LoadingController, ModalController, NavController, NavParams, Platform, ToastController, ViewController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { UserData } from '../../../providers/user-data';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Storage } from '@ionic/storage';
import * as moment from 'moment-timezone';

@Component({
  selector: 'custom-page-add',
  templateUrl: 'page-add.html'
})
export class CreatePage {
  page: any = {};
  pageId: string;
  universityId: string;

  constructor(
    public af: AngularFire,
    public app: App,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public userData: UserData,
    public storage: Storage,
    public afoDatabase: AngularFireOfflineDatabase
  ) {
    this.pageId = navParams.get('pageId');
    this.storage.get('universityId')
      .then((universityId: string) => {
        this.universityId = 'universitiesData/' + universityId;
        if (this.pageId) {
          afoDatabase.object(this.universityId + '/pages/' + this.pageId)
            .subscribe((page) => {
              this.page = Object.assign({}, page);
            });
        } else {
          this.userData.getUid().then((value) => {
            this.page.createdBy = value;
          });
        }
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  savePage() {
    let loading = this.loadingCtrl.create({
      content: 'saving wait ...'
    })
    loading.present();
    new Promise((resolve, reject) => {
      if (this.pageId) {
        delete this.page.$exists;
        delete this.page.$key;
        this.page.modifiedDate = (+ moment.utc().format('x'));

        this.af.database.object(this.universityId + '/pages/' + this.page.id)
          .set(this.page)
          .then(() => {
            resolve('updated');
          });
      } else {
        this.page.id = this.page.createdDate = (+ moment.utc().format('x'));

        this.af.database.object(this.universityId + '/pages/' + this.page.id)
          .set(this.page)
          .then(() => {
            resolve('created');
          });
      }
    })
      .then((msg) => {
        loading.dismiss();
        this.toastCtrl.create({
          message: 'Page ' + msg + ' successfully',
          duration: 2000
        }).present();
        this.navCtrl.pop();
      });
  }
}
