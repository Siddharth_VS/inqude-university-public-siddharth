import { Component, ViewChild } from "@angular/core";
import { Content, ModalController, NavController, NavParams, Searchbar } from "ionic-angular";
import { EventDetailPage } from "../event-detail/event-detail";
import { HelperService } from "../../providers/helperService";
import { AngularFireOfflineDatabase } from "angularfire2-offline/database";
import { ApiService } from "../../providers/crm/apiService";
import { Talisma } from "../../providers/crm/talisma";
import { ScheduleFormPage } from "../schedule/schedule-form/schedule-form";
import * as _ from "underscore";
import * as moment from "moment-timezone";

@Component({
  selector: "page-event-list",
  templateUrl: "event-list.html"
})
export class EventListPage {
  @ViewChild(Content) eventList: Content;

  events: any[] = [];
  segment: any = "live";
  live: any = [];
  past: any = [];
  uid: any;
  roleValue: any;
  allEvents: any = [];
  loadedEvents: any[] = [];
  loadedEventsCount: number = 0;
  loadedLiveEventsCount: number = 0;
  loadedPastEventsCount: number = 0;
  eventsPerPage: number = 10;
  timeZone: string = "America/Matamoros";
  reachedMaxLimit: boolean = false;
  query: string = "";
  hideSearchBar: boolean = true;
  eventIds: any[] = [];
  searchEnabled: boolean = false;
  subURI: string = "";
  timeoutId: any;
  hideBackButton: boolean;
  moment = moment;
  @ViewChild("searchBar") searchBar: Searchbar;

  constructor(
    public helperService: HelperService,
    private _nav: NavController,
    private modalCtrl: ModalController,
    private navParams: NavParams,
    public apiService: ApiService,
    public afoDatabase: AngularFireOfflineDatabase,
    public talisma: Talisma
  ) {
    this.navParams.get("hideBackButton") ? (this.hideBackButton = this.navParams.get("hideBackButton")) : (this.hideBackButton = true);
  }

  ionViewDidEnter() {
    this.afoDatabase
      .object("/settings/timeZone")
      .take(1)
      .subscribe((timeZone: any) => {
        this.timeZone = timeZone.$value;
        this.init();
      });
  }

  init(ionRefresher?: any) {
    let self = this;
    if (!ionRefresher) {
      this.helperService.showLoading();
    }
    new Promise((resolve) => {
      this.helperService.getRoleValue().then((roleValue: any) => {
        self.roleValue = roleValue;
        resolve();
      });
    }).then(() => {
      if (this.roleValue == 99) {
        let temp = {
          loadedLiveEventsCount: this.loadedLiveEventsCount,
          loadedPastEventsCount: this.loadedPastEventsCount,
          loadedEvents: this.loadedEvents
        };
        this.getAllEvents()
          .then(() => {
            if (ionRefresher) {
              ionRefresher.complete();
            } else {
              this.helperService.hideLoading();
            }
          })
          .catch((err) => {
            console.log("err occured", err);
            this.helperService.hideLoading();
            this.loadedLiveEventsCount = temp.loadedLiveEventsCount;
            this.loadedPastEventsCount = temp.loadedPastEventsCount;

            this.loadedEvents = _.sortBy(temp.loadedEvents, function(event) {
              return +moment(event.EventStartDate).format("x");
            });
          });
      } else {
        this.helperService.hideLoading();
      }
    });
  }

  onIonClear() {
    this.query = "";
    this.searchEnabled = false;
    this.reachedMaxLimit = false;
    this.loadedEvents = [];
    this.loadedEventsCount = 0;
    this.loadedLiveEventsCount = 0;
    this.loadedPastEventsCount = 0;
    this.toggleNavBar();
    this.loadEvents();
  }

  toggleNavBar() {
    this.hideSearchBar = !this.hideSearchBar;
    setTimeout(() => {
      if (this.searchBar) {
        this.searchBar.setFocus();
      }
    });
  }

  getAllEvents() {
    this.loadedLiveEventsCount = 0;
    this.loadedPastEventsCount = 0;
    return new Promise((resolve) => {
      this.getEvents(this.segment).then((response: any) => {
        if (response.length) {
          this.segment == "live" ? (this.live = response) : (this.past = response);
          this.loadedEvents = _.sortBy(response, function(event: any) {
            return +moment(event.EventStartDate).format("x");
          });
        }
        resolve();
      });
    });
  }

  updateList(segment: any) {
    this.reachedMaxLimit = false;
    if (!this.hideSearchBar) {
      this.query = "";
      this.searchEnabled = false;
      this.toggleNavBar();
    }
    if (segment.value == "live") {
      this.loadedEvents = _.sortBy(this.live, function(event: any) {
        return +moment(event.EventStartDate).format("x");
      });
      this.loadedLiveEventsCount = this.loadedEvents.length;
    } else if (segment.value == "past") {
      this.helperService.showLoading();
      this.loadedPastEventsCount = 0;
      this.loadedLiveEventsCount = 0;
      this.getEvents("past")
        .then((events: any) => {
          this.past = events;
          this.loadedPastEventsCount = events.length;
          this.loadedEvents = _.sortBy(this.past, function(event: any) {
            return +moment(event.EventStartDate).format("x");
          });
          this.helperService.hideLoading();
        })
        .catch((err) => {
          this.helperService.hideLoading();
          console.log("err occured", err);
        });
    }
    this.eventList.scrollToTop();
  }

  public viewEvent(eventData: any) {
    this._nav.push(EventDetailPage, { event: eventData });
  }

  loadEvents(infiniteScroll?: any) {
    setTimeout(() => {
      this.getEvents(this.segment, this.searchEnabled ? this.subURI : "").then((events: any) => {
        this.loadedEvents = _.sortBy(this.loadedEvents.concat(events), function(event: any) {
          return +moment(event.EventStartDate).format("x");
        });
        if (infiniteScroll) {
          infiniteScroll.complete();
        }
      });
    }, 100);
  }

  onInput() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId);
      if (this.query && this.query.length) {
        this.loadedEvents = [];
        this.reachedMaxLimit = false;
        this.loadedLiveEventsCount = 0;
        this.loadedPastEventsCount = 0;
        this.helperService.showLoading();
        this.searchEnabled = true;
        this.subURI = " and contains(Name,'" + this.query + "')";
        this.getEvents(this.segment, this.subURI).then((events: any) => {
          this.loadedEvents = _.sortBy(events, function(event: any) {
            return +moment(event.EventStartDate).format("x");
          });
          this.helperService.hideLoading();
        });
      } else {
        this.reachedMaxLimit = false;
        this.loadEvents();
      }
    }, 500);
  }

  getEvents(segment: string, query?: string) {
    return new Promise((resolve, reject) => {
      let subUrl =
        "$filter=(EventStartDate" +
        (segment == "live" ? " ge " : " lt ") +
        (moment()
          .tz(this.timeZone)
          .subtract(1, "days")
          .endOf("day")
          .format("") +
          ") ");
      subUrl = subUrl.concat((query && query.length ? "" + query + "&" : "&") + "$orderby=EventStartDate" + (segment == "live" ? " asc" : " desc "));
      this.talisma.events
        .getEvents(this.eventsPerPage, segment == "live" ? this.loadedLiveEventsCount : this.loadedPastEventsCount, subUrl)
        .then((response: any) => {
          if (response.length) {
            segment == "live" ? (this.loadedLiveEventsCount += response.length) : (this.loadedPastEventsCount += response.length);
            resolve(response);
          } else {
            this.reachedMaxLimit = true;
            resolve([]);
          }
        })
        .catch((err) => {
          console.log("err occured", err);
          reject(err);
        });
    });
  }

  getClass(event: any) {
    return moment(event.EventStartDate)
      .tz(this.timeZone)
      .format("x") >=
      moment()
        .tz(this.timeZone)
        .subtract(1, "days")
        .endOf("day")
        .format("x")
      ? ""
      : "past-event";
  }

  add() {
    let modal = this.modalCtrl.create(ScheduleFormPage);
    modal.present();

    modal.onWillDismiss(() => {
      this.init();
    });
  }
}
