import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'alert-modal',
  templateUrl: 'alert-modal.html'
})

export class AlertModal {
  status = '';
  message:string;
  showBackButton: boolean = false;
  successAudio = new Audio('assets/sound/scanning_success.mp3');
  failureAudio = new Audio('assets/sound/scanning_error.mp3');
  constructor(
    public view: ViewController,
    public params: NavParams
  ) {
    this.status = params.get('status');
    this.message = params.get('message');
    this.status == 'success' ? this.successAudio.play() : this.failureAudio.play();
    setTimeout(() => {
      this.dismiss();
    }, 3500);
  }

  dismiss() {
    this.view.dismiss();
  }

  getClass() {
    return this.status == 'success' ? 'success' : 'failure';
  }

  getStatusText() {
    let successMessage = 'The Participant has been successfully checked-in !';
    let failureMessage = 'Something went wrong!<br> Unable to add Participant';
    if(this.message){
      return this.message;
    }
    return this.status == 'success' ? successMessage : failureMessage;
  }
}
