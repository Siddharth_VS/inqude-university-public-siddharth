import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { UserData } from '../../../providers/user-data';
import { UserMangementPage } from '../user-management/user-management';
import * as _ from 'underscore';

@Component({
  selector: 'page-user-listing',
  templateUrl: 'user-listing.html',
  providers: [UserData]
})

export class UserListingPage {
  users: any[] = [];
  uid: string;
  query: string = '';
  filterKeys: any = ["firstName", "lastName", "uid"];

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public modalCtrl: ModalController,
    public userData: UserData
  ) {
    this.userData.getUid().then((uid) => {
      this.uid = uid;
      this.afoDatabase.list('/users', { query: { orderByChild: 'firstName' } })
        .subscribe((snapshots) => {
          let temp: any[] = [], pos: number;
          for (let snapshot of snapshots) {
            if (snapshot.$key && snapshot.$key.length && snapshot.status == 'accepted') {
              pos = (_.findIndex(temp, { '$key': snapshot.$key }))
              if (pos == -1) {
                temp.push(snapshot);
              }
            }
          }
          this.users = temp;
        })
    })
  }

  openModal(uid: string) {
    let modal = this.modalCtrl.create(UserMangementPage, { userUid: uid });
    modal.present();
  }

}
