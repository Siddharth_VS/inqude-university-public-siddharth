import { Component, ViewChild } from "@angular/core";
import { LoadingController, NavController, Searchbar } from "ionic-angular";
import { AngularFireOfflineDatabase } from "angularfire2-offline";
import { UserData } from "../../providers/user-data";
import { SendInvite } from "../invites/send-invite/send-invite";
import { HelperService } from "../../providers/helperService";
import { ModeratorList } from "../moderator-list/moderator-list";
import * as _ from "underscore";

@Component({
  selector: "page-user-list",
  templateUrl: "user-list.html",
  providers: [SendInvite]
})
export class UserList {
  users: any = [];
  uid: any;
  pushPage: any;
  moderatorPage: any;
  query: string = "";
  hideSearchBar: boolean = true;
  filterKeys: any = ["firstName", "lastName", "email"];
  userListlength: number;

  @ViewChild("searchBar") searchBar: Searchbar;
  @ViewChild("userList") userList: any = [];

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public sendInvite: SendInvite,
    public userData: UserData
  ) {
    this.pushPage = SendInvite;
    this.moderatorPage = ModeratorList;

    this.helperService.getUid().then((value: any) => {
      this.uid = value;
      this.afoDatabase.list("/users", { query: { orderByChild: "uid", equalTo: this.uid } }).subscribe((snapshots: any) => {
        let temp: any[] = [];
        // pos: number;
        for (let snapshot of snapshots) {
          if (snapshot.$key && snapshot.$key.length && snapshot.status) {
            temp.push(snapshot);
            // pos = (_.findIndex(temp, { '$key': snapshot.$key }))
            // if (pos == -1 && snapshot.uid !== this.uid) {
            //   temp.push(snapshot);
            // }
          }
        }
        this.users = _.uniq(Object.assign([], _.sortBy(temp, "firstName")), function(user) {
          return user.uid;
        });
      });
    });
  }

  onIonClear() {
    this.query = "";
    this.toggleNavBar();
  }

  toggleNavBar() {
    this.hideSearchBar = !this.hideSearchBar;
    setTimeout(() => {
      if (this.searchBar) {
        this.searchBar.setFocus();
      }
    });
  }

  resendInvite(user: any) {
    this.helperService.showLoading();
    user.isUserExists = true;
    this.sendInvite.sendMail(user);
  }

  viewSendInivitePage() {
    this.helperService.showMessage("Adding additional moderators has been disabled for this version", 3000);
    // this.navCtrl.push(SendInvite);
  }
}
