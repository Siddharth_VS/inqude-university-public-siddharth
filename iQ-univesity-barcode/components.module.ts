import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgForm } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ButtonsComponent } from './buttons/buttons.component';
import { ComponentsRoutes } from './components.routing';
import { GridSystemComponent } from './grid/grid.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { PanelsComponent } from './panels/panels.component';
import { SweetAlertComponent } from './sweetalert/sweetalert.component';
import { TypographyComponent } from './typography/typography.component';
import { HomeworkComponent } from './homework/homework.component';
import { MatDialogModule, MatSelectModule, MatAutocompleteModule, MatExpansionModule, MatInputModule } from '@angular/material';
import { HelperService } from '../services/helper.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComponentsRoutes),
    FormsModule,
    MatDialogModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatInputModule
  ],
  declarations: [
      ButtonsComponent,
      GridSystemComponent,
      IconsComponent,
      NotificationsComponent,
      PanelsComponent,
      SweetAlertComponent,
      TypographyComponent,
      HomeworkComponent
  ],
  providers:[
    HelperService,
    NgForm
  ]
})

export class ComponentsModule {}
