import { Directive, Input, ElementRef, Renderer, OnChanges } from '@angular/core';

@Directive({
  selector: '[poll-vote]'
})

export class PollProgressBar implements OnChanges {
  constructor(
    public el: ElementRef,
    public renderer: Renderer, ) { }
  @Input('poll-vote') vote: any;
  @Input('vote-array') vote_array: any;
  total_votes: any = 0;
  percentage: any;
  ngOnChanges() {
    for (let i = 0; i < this.vote_array.length; i++) {
      this.total_votes += this.vote_array[i];
    }
    if (this.total_votes > 0) {
      this.percentage = ((this.vote / this.total_votes) * 100);
      this.renderer.setElementStyle(this.el.nativeElement, 'width', this.percentage + '%');
      this.el.nativeElement.querySelector('.votes').innerHTML = this.percentage.toFixed(0) + '%';
    }
    else {
      if (this.el.nativeElement.parentNode) {
        this.el.nativeElement.parentNode.innerHTML = "<div class='no-votes'> No. votes</div>";
      }
    }
  }
}
