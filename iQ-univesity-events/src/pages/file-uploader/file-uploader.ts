import { Component } from '@angular/core';
import { NavParams, ViewController, ModalController, ToastController, LoadingController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { HelperService } from '../../providers/helperService';
import { UserData } from '../../providers/user-data';
import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'page-file-uploader',
  templateUrl: 'file-uploader.html',
  providers: [HelperService]
})

export class FileUploader {
  file = {
    name: "",
  };
  path: string;
  folder: string = '';
  fileName: string = '';
  files: any = [];
  uploadType: any;
  selectId: number = -1;
  allowMultipleSelection: boolean = false;
  selectedFiles: any[] = [];
  universityRef: any = '';

  constructor(
    public af: AngularFire,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public helper: HelperService,
    public toastCtrl: ToastController,
    public userData: UserData,
    public loadingCtrl: LoadingController
  ) {
    this.uploadType = this.navParams.get("uploadType");
    this.folder = this.navParams.get('folder');
    this.fileName = this.navParams.get('fileName');
    this.path = this.uploadType == 'file' ? 'files' : 'images';

    if (this.navParams.get('allowMultipleSelection')) {
      this.allowMultipleSelection = this.navParams.get('allowMultipleSelection');
    }
    let self: any = this;
    this.userData.getUniversityId()
      .then((universityId) => {
        self.universityRef = 'universitiesData/' + universityId;
        this.af.database.object(self.universityRef + '/' + self.path + '/' + self.folder, { preserveSnapshot: true })
          .subscribe(snapshots => {
            let temp: any[] = [];
            if (self.uploadType == 'file') {
              snapshots.forEach((snapshot: any) => {
                temp.push(snapshot.val());
              });
              self.files = _.sortBy(temp, function(file) {
                return (+ moment(file.modifiedTime).format('x'));
              }).reverse();
            }
            else {
              snapshots.forEach((snapshot: any) => {
                temp.push({ "url": snapshot.val() });
              });
              temp.reverse();
              self.files = temp;
            }
          });
      })
  }

  dismiss(selectedId: any) {
    let temp: any = [];
    let self = this;
    this.selectedFiles.forEach((index) => {
      self.uploadType == 'file' ? temp.push(this.files[index].id) : temp.push(this.files[index].url);
    })
    if (this.allowMultipleSelection) {
      this.viewCtrl.dismiss(temp);
    }
    else {
      if ((temp.length > 0) && temp[0]) {
        this.viewCtrl.dismiss(temp[0]);
      } else {
        this.viewCtrl.dismiss();
      }
    }
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  onClick(index: number) {
    if (this.allowMultipleSelection) {
      let imageIndex = this.selectedFiles.indexOf(index);
      if (imageIndex < 0) {
        this.selectedFiles.push(index);
      }
      else {
        this.selectedFiles.splice(imageIndex, 1);
      }
    }
    else if (this.selectedFiles[0] === index) {
      this.selectedFiles.splice(0, 1);
    } else {
      this.selectedFiles[0] = index;
    }
  }

  autoSelectFile(obj: any) {
    if (this.uploadType == 'file') {
      this.selectedFiles[0] = 0;
    }
    else {
      let imageIndex: number;
      for (let i = 0; i < this.files.length; i++) {
        if (this.files[i].url == obj.data) {
          imageIndex = i;
        }
      }
      this.selectedFiles[0] = imageIndex;
    }
  }

  getClass(index: number) {
    return this.selectedFiles.indexOf(index) > -1 ? 'active_img' : '';
  }
}
