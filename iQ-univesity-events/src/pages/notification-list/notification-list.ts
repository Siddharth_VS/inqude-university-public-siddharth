import { Component } from '@angular/core';
import { LoadingController, MenuController, ModalController, NavController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Storage } from '@ionic/storage';
import { LinkService } from '../../providers/linkService';
import { UserData } from '../../providers/user-data';
import * as _ from 'underscore';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notification-list.html',
  providers: [LinkService]
})

export class NotificationsListPage {
  notifications: any = [];
  roleValue: any;
  universityRef: string;
  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public loadingCtrl: LoadingController,
    public linkService: LinkService,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public storage: Storage,
    public userData: UserData
  ) { }

  ngOnInit() {
    let loading = this.loadingCtrl.create();
    loading.present();
    //enabling the sidemenu with swipe
    this.menuCtrl.enable(true);
    this.userData.getUniversityId()
      .then((universityId) => {
        this.universityRef = 'universitiesData/' + universityId;
        return this.userData.getUid()
      })
      .then((uid) => {
        this.afoDatabase.object('/users/' + uid)
          .subscribe((user) => {
            this.afoDatabase.list('/eventNotifications')
              .subscribe((res) => {
                this.notifications = [];
                loading.dismiss();
                if (res.length > 0) {
                  let userNotifications = _.filter(res, function(obj: any) { return (obj.target == "All Devices" ||( Object.keys(user.notifications || []).indexOf(obj.id) >= 0) && obj.recieverApp == 'Student') })
                  this.notifications = _.sortBy(userNotifications, function(obj: any) { return +(obj.createdDate) }).reverse()
                }
                else {
                  this.notifications = [];
                }
              });
          })
      })
  }

  viewNotification(notification: any) {
    if (notification.route) {
      this.linkService.viewPage(notification.route);
    }
  };
}
