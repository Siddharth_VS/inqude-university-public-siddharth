import { Component } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';
import { HelperService } from '../../providers/helperService';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Talisma } from '../../providers/crm/talisma';
import { EventFilter } from '../../components/event-filter/event-filter';
import * as _ from 'underscore';
import * as moment from 'moment';
import { EventDetailPage } from '../event-detail/event-detail';

@Component({
  selector: 'page-my-events',
  templateUrl: 'my-events.html'
})

export class MyEventsPage {
  hideSearchBar: boolean = true;
  query: string = '';
  eventDataFields: any = "Name,EventId,EventStartDate,EventCity,EventVenue,Notes,AvailableSeats,RegisteredParticipants,EventCost";
  liveEvents: any[] = [];
  pastEvents: any[] = [];
  allEvents: any[] = [];
  uid: any;
  segment: any = 'live';
  events: any[] = [];
  timeoutId: any;
  filterKeys: any = ['Name', 'EventVenue', 'Notes'];
  _ = _;
  moment = moment;
  universityId: string;
  timeZone:any;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public talisma: Talisma,
  ) {  }

  ionViewDidEnter(){
    this.helperService.getUid()
      .then((uid: any) => {
        this.uid = uid;
        return this.helperService.getUniversityId()
      })
      .then((universityId) => {
        this.universityId = '/universitiesData/' + universityId;
        this.afoDatabase
          .object("settings/timeZone")
          .take(1)
          .subscribe((zone: any) => {
            this.timeZone = zone.$value;
            this.loadEvents();
          });
      })
  }

  presentPopover(myEvent: any) {
    let popover = this.popoverCtrl.create(EventFilter, { filter: '' });
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss((result: any) => {
      // if (result) {
      //   this.segment = result;
      //   this.toggleSelection();
      // }
    })
  }

  toggleSelection() {
    this.events = [];
    this.loadEvents();
  }

  loadEvents() {
    let self=this;
    this.helperService.showLoading();
    this.afoDatabase.object('/users/' + this.uid + '/participationStatus')
      .subscribe((data: any) => {
        let userEvents = _.keys(data);
        this.afoDatabase.object(this.universityId + '/events/')
          .subscribe((events: any) => {
            this.allEvents =[];
            let filteredEvents:any=[];
             _.each(events, function(event: any) {
              if(userEvents.indexOf('' + event.EventId) > -1){
                event.EventStartDate = moment.utc(event.EventStartDate).tz(self.timeZone).format('');
                event.EventEndDate = moment.utc(event.EventEndDate).tz(self.timeZone).format('');
                self.allEvents.push(event);
              };
            })
            return this.filterEvents()
              .then(() => {
                this.helperService.hideLoading();
              })
          })
      })
  }

  getCurrentEvents() {
    this.events = (this.segment == 'live') ? this.liveEvents : this.pastEvents;
  }

  filterEvents() {
    let self = this;
    return new Promise((resolve) => {
      let temp = _.partition(this.allEvents, function(event: any) {
        if (event.EventStartDate >= self.helperService.getTimeStamp()) {
          return event.EventId;
        }
      });
      this.liveEvents = temp[0];
      this.pastEvents = temp[1];
      this.events = this.segment == 'live' ? temp[0] : temp[1];
      resolve();
    })
  }

  groupByDate(events: any[]) {
    return _.groupBy(events, (event: any) => { return moment(event.EventStartDate).format('YYYYMMDD') })
  }

  groupByMonth(events: any[]) {
    return _.groupBy(events, (event: any) => { return moment(event.EventStartDate).format('MMM YYYY') })
  }
  toggleNavBar() {
    this.hideSearchBar = !this.hideSearchBar;
  }

  onIonClear() {
    this.query = '';
    this.events = [];
    this.toggleNavBar();
    this.loadEvents();
  }

  onInput() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId)
    }
    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId)
      this.events = this.allEvents;
    }, 500);
  }

  viewEvent(eventData: any) {
    this.navCtrl.push(EventDetailPage, { event: eventData });
  }

}
