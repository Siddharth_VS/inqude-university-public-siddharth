import { Component } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';

@Component({
  selector: 'edit-moderator',
  templateUrl: 'edit-moderator.html'
})

export class EditModerator {
  user: any = {};

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
  ) {
    let self = this;
    this.helperService.getUid()
      .then((uid: any) => {
        self.afoDatabase.object('/users/' + uid)
          .subscribe((snapshot: any) => {
            this.user = snapshot;
          })
      });
  }
}
