import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { App } from 'ionic-angular';
import { SpeakerDetail } from '../../pages/speakers/speaker-detail';

@Component({
  selector: 'persona',
  templateUrl: 'persona.html'
})
export class Persona {
  @Input('params') args: any;
  @Output() removePersona = new EventEmitter();
  persona?: any[] = [];
  universityRef: string;
  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    public helper: HelperService
  ) { }

  ngOnChanges() {
    let self = this;
    this.helper.getUniversityId()
      .then((universityId: string) => {
        this.universityRef = 'universitiesData/' + universityId;
        self.afoDatabase.object(this.universityRef + '/speakers/' + this.args.id)
          .subscribe((speaker) => {
            self.persona = speaker;
          });
      });
  }

  viewPersona(persona: any) {
    if (this.args.view == true) {
      this.app.getRootNav().push(SpeakerDetail, { speakerId: persona.id });
    }
  }

  deletePersona(personaId: any) {
    this.removePersona.emit({ id: personaId, type: this.args.type });
  }
}
