import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EventListPage } from '../../pages/event-list/event-list';
import { EventDetailPage } from '../../pages/event-detail/event-detail';
import { MyEventsPage } from '../../pages/my-events/my-events';

@Component({
  selector: 'footer-tabs',
  templateUrl: 'footer-tabs.html'
})

export class FooterTabs {
  tabs: any = [];

  constructor(
    private navCtrl: NavController
  ) {
    this.tabs = [{
      "icon": "md-calendar",
      "title": "All Events",
      "value": EventListPage
    }, {
      "title": "My Events",
      "icon": "ios-star-outline",
      "value": MyEventsPage
    }];
  }

  viewEvent(eventData: any) {
    this.navCtrl.push(EventDetailPage, { event: eventData });
  }

  isActive(value: any) {
    return true;
  }
  viewtab(value: any) {
    this.navCtrl.setRoot(value)
  }
}
