import { Component, Input } from '@angular/core';
import { UtilProvider } from '../../providers/util-provider';

@Component({
  selector: 'social-links',
  templateUrl: 'social-links.html'
})

export class SocialLinksComponent {
  hideSocialLinks: boolean = true;

  @Input('data') data: any;

  constructor(
    private util: UtilProvider
  ) { }

  isExists(data: any) {
    return !this.util.isEmpty(data || '');
  }

  ngOnChanges() {
    if ((
      !this.util.isEmpty(this.data.facebook) ||
      !this.util.isEmpty(this.data.twitter) ||
      !this.util.isEmpty(this.data.website) ||
      !this.util.isEmpty(this.data.instagram))
    ) {
      this.hideSocialLinks = false;
    } else {
      this.hideSocialLinks = true;
    }
  }

}
