import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EventDetailPage } from '../../pages/event-detail/event-detail';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import * as moment from 'moment-timezone';

@Component({
  selector: 'single-event',
  templateUrl: 'single-event.html'
})

export class SingleEvent {
  @Input('params') event: any;
  eventData: any;
  timeZone: string = '';
  moment = moment;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    private navCtrl: NavController
  ) {
  }

  ngOnChanges(){
    this.afoDatabase
      .object("settings/timeZone")
      .take(1)
      .subscribe((timeZone: any) => {
        this.timeZone = timeZone.$exists() ? timeZone.$value : '';
        if (this.timeZone) {
          this.eventData = this.event;
        }
      });
  }

  getClass(eventData: any) {
    if (this.timeZone && this.eventData) {
      return ((moment(this.eventData.EventStartDate).tz(this.timeZone).format('x')) >= (moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('x'))) ? '' : 'past-event';
    }
  }

  viewEvent(eventData: any) {
    this.navCtrl.push(EventDetailPage, { event: eventData });
  }
}
