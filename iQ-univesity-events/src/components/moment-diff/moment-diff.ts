import { Component, Input } from '@angular/core';
import moment from 'moment';

@Component({
  selector: 'moment-diff',
  templateUrl: 'moment-diff.html'
})
export class MomentDiff {
  @Input('startTime') startTime: any;
  @Input('endTime') endTime: any;
  timeDiffInWords: string = '';

  constructor() { }

  ngOnChanges() {
    let endTime = moment(this.endTime);
    let startTime = moment(this.startTime);
    let hours: any = endTime.diff(startTime, 'hours');
    let min: any = moment.utc(moment(this.endTime, "HH:mm:ss").diff(moment(this.startTime, "HH:mm:ss"))).format("mm");
    hours = hours ? (hours + ' hr ') : '';
    min = parseInt(min) ? (min + ' min') : '';
    this.timeDiffInWords = hours + min;
  }
}
